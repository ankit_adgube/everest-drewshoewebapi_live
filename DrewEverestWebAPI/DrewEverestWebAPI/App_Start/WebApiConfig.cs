﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace DrewEverestWebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
               name: "CreateAddress",
               routeTemplate: "api/EverestAPI/CreateAddress/{addressxml}",
               defaults: new { controller = "EverestAPI", action = "CreateAddress", addressxml = RouteParameter.Optional }
           );
            //   config.Routes.MapHttpRoute(
            //    name: "TestAPI",
            //    routeTemplate: "api/EverestAPI/TestAPI/",
            //    defaults: new { controller = "EverestAPI", action = "TestAPI" }
            //);
        }
    }
}
