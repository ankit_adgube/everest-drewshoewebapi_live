﻿using DrewEverestWebAPI.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using System.Xml;
using System.Xml.Linq;
using Znode.Engine.Api.Models;

namespace DrewEverestWebAPI.Controllers
{
    public class EverestAPIController : BaseController
    {
        //[Route("api/EverestAPI/CreateAddress/{addressxml}")]
       [HttpPost]
        public string CreateAddress(AddressModel znodeAddressModel)
        {
            try
            {
                EverestAPIService service = new EverestAPIService();
                string result = service.CreateAddress(znodeAddressModel);
                return result;
            }
            catch (Exception ex)
            {
                //return "CreateAddress Error : Message = " + ex.Message + " InnerException=" + ex.InnerException + " Stack Trace : " + ex.StackTrace;
                return ex.Message;

            }

        }
        
        [HttpGet]
        public string TestAPI()
        {
            try
            {
                EverestAPIService service = new EverestAPIService();
                service.Log("Checking Logs");
                return "Hello Welcome to Drew Everest API!";
            }
            catch (Exception ex)
            {
                return "Error : " + ex.InnerException + " Stack Trace : " + ex.StackTrace;
            }
        }

        [HttpPost]
        public string CreateSaleOrder(OrderModel model)
        {
            try
            {
                EverestAPIService service = new EverestAPIService();
                string result = service.CreateSaleorder(model);
                return result;
            }
            catch (Exception ex)
            {
                return "CreateSaleOrder Error :  Message = " + ex.Message + " InnerException=" + ex.InnerException + " Stack Trace : " + ex.StackTrace;
            }
        }
        [HttpPost]
        public string UpdateSaleOrder(OrderModel model)
        {
            try
            {
                EverestAPIService service = new EverestAPIService();
                string result = service.UpdateSaleorder(model);
                return result;
            }
            catch (Exception ex)
            {
                return "UpdateSaleOrder Error : Message = " + ex.Message + " InnerException=" + ex.InnerException + " Stack Trace : " + ex.StackTrace;
            }
        }
        [HttpPost]
        public string CancelSaleOrder(OrderModel model)
        {
            try
            {
                EverestAPIService service = new EverestAPIService();
                string result = service.CancelSaleorder(model);
                return result;
            }
            catch (Exception ex)
            {
                return "CancelSaleOrder Error : Message = " + ex.Message + " InnerException=" + ex.InnerException + " Stack Trace : " + ex.StackTrace;
            }
        }

        [HttpPost]
        public string CreateAddressAndOrder(Tuple<AddressModel, OrderModel> znodeModels)
        {
            try
            {
                AddressModel znodeAddressModel = znodeModels.Item1;
                OrderModel znodeOrderModel = znodeModels.Item2;
                EverestAPIService service = new EverestAPIService();
                string result = service.CreateAddressAndOrder(znodeAddressModel, znodeOrderModel);
                return result;
            }
            catch (Exception ex)
            {
                return "CreateAddress Error : Message = " + ex.Message + " InnerException=" + ex.InnerException + " Stack Trace : " + ex.StackTrace;
            }
        }

    }
}